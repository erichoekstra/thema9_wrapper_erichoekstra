import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

/**
 * Class to alter the given script by a user using r
 *
 * @author Eric Hoekstra
 */
public class ModifyFile {
    /**
     * Method to modify given data by a user.
     *
     * @param unknownFile: Unknown file given by a user.
     */
    protected void modifyData(String unknownFile) throws IOException, InterruptedException {
        System.out.println(unknownFile);
        ProcessBuilder pb = new ProcessBuilder("Rscript", "R_script.R",
                unknownFile);
        //pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        //pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        Process p = pb.start();

        // Can be uncommented to see R output
        //BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        //String readline;
        //int i = 0;
        //while ((readline = reader.readLine()) != null) {
        //    System.out.println(++i + " " + readline);
        //}

        // R needs some time to create the arff. Otherwise the next java code will not recognize the arff file.
        TimeUnit.SECONDS.sleep(5);

        System.out.println("DONE");
    }
}
