**Thema 9 - Java Weka Wrapper - Eric Hoekstra - Version 1.0**

This repo contains the java wrapper to classify instances given by a user. It will classify what type of systemic scleroris someone has based on antibody data the user will give as input. 


---

## Dependencies

The dependencies below are configured in the build.gradle file. As long as this build.gradle file is used there should be no problem.
 
Dependencies:  
- Java version 11 or higher is neccessary to run this script.  
- Apache CLI version 1.4  
- Weka API version 3.8.0  

## Example input data
This tool will expect antibody data of certain antibodies. It's not neccessary to give each antibody but it's recommended for the best results.  

| AAR_CT_ELIS   | AER_CT_ELISA  | Ro52_EL_SC  | PDGFR_EL_SC |
| ------------- |:-------------:| -----:|3| 
| 8.15      | 5.34 | 3 |53| 
| 7.24      | 2.34      |   2 |4|
| 3.34 | 8.43      |    1 |1| 

For the full file check the unknowninstances.csv in the build/lib folder. You don't have to include a colom with 'diagnosis', this will be done automatically.

## Set-Up
Only csv files are supported. Because of the high amount of columns it's not efficient to pass it via the command line.

1. Clone the repo via the command line onto your own pc
2. The jar file can be found in Thema9_Wrapper_EricHoekstra/build/libs
3. Drag the .csv file of your choice into the Thema9_Wrapper_EricHoekstra/build/libs folder.
4. Navigate, on the terminal, to the Thema9_Wrapper_EricHoekstra/build/libs folder.
5. Run the script like this:  

```bash
~ libs $ java -jar -f <file-name>
```

6. For help run it like this:  

```bash
~ libs $ java -jar -h
```

---

## The output

The output with the classified labels will be printed on the terminal and look like this:

![](example_output.png)

It will be shown as a comma seperated file. At the end of each row the classification for this instance will be showed.

Next to the output on the screen it will also be written to a file. This file is called classification.csv and can be found in the build/libs folder.

---

## Contact

For contact: e.j.hoekstra@st.hanze.nl